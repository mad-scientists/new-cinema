# New-Cinema Problem Solver

## Running

Launch the Cinema Assignment with 2 input arguments(strings):
* The cinema (including groups to place)
* Which solver to use

For a reference to how the file should look like, see any file in the "testcases" folder.
Example: `../../../../../testcases/reference/offline/Exact01.txt`

The possible solver to choose from as second argument:
* `online-greedy`
* `online-naive`
* `off-bb`
* `off-ilp`

## Setup

NOTE: for the "off-ilp" solver, there are certain packages required, run the following commands to install them from the commandline (directory should be this root directory)

```
virtualenv -p python3 .venv
source .venv/bin/activate
pip install -r ./requirements.txt
```

## Z3 Solver

To run the z3 based SMT solver, install the requirements as mentioned above and run `./prototypes/z3solver.py <FILE>`