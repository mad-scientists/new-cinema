This currently still uses Python 2, as the Python 3 bindings don't come out of the box with NixOS.
Maybe you can also make it work with Python 3.

Tested with:
- Gurobi 8.1.0
- CBC 2.10.3 (usually much slower)

*Warning*: On some installations of Gurobi, it doesn't allow preventing it from printing license information.
This might interfere with the solution output.

```
python cinema-ilp.py --help
```
