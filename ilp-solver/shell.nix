{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  buildInputs = with pkgs; [
    python
    pythonPackages.enum-compat
    gurobi
    pythonPackages.gurobipy
    or-tools
    pythonPackages.ortools
  ];
}
