#!/usr/bin/env python
from __future__ import print_function
import argparse
import copy
from enum import Enum
import sys

verbose_mode = False

def debug(*args):
    if verbose_mode:
        print(*args)


class InvalidCinema(Exception):
    def __init__(self, reason=None):
        self.reason = reason

    def __str__(self):
        return "Invalid cinema: " + self.reason

class InvalidSolution(Exception):
    def __init__(self, reason=None):
        self.reason = reason

    def __str__(self):
        return "Invalid solution: " + self.reason


class Item(Enum):
    # Representing each possible item.
    EMPTY = '0'
    CHAIR = '1'
    TAKEN = 'x'


class Grid(object):
    def __init__(self, width, height, default_value=[]):
        self.default_value = default_value
        self.height = height
        self.width = width
        self.grid = [
            [self.default_value for x in range(width)] for y in range(height)
        ]

    def positions(self):
        return [(x, y) for x in range(self.width) for y in range(self.height)]

    def at(self, x, y):
        # Abstraction to allow handling out of bounds cases.
        # We assume if we are out of bounds, the position is empty.
        if x < 0 or y < 0 or x >= self.width or y >= self.height:
            return self.default_value
        else:
            return self.grid[y][x]

    def set(self, x, y, value, places=1):
        for i in range(places):
            self.grid[y][x+i] = value


class CinemaProblem(Grid):
    def __init__(self, input_handle):
        self.groups = {}
        width, height = self._read_dimensions_from_handle(input_handle)
        super(CinemaProblem, self).__init__(width, height, default_value=Item.EMPTY)
        self._read_cinema_from_handle(input_handle, width, height)
        self._read_groups_from_handle(input_handle)

    def _read_dimensions_from_handle(self, handle):
            # Read in the number of rows and columns.
            height = int(handle.readline())
            width = int(handle.readline())
            return (width, height)

    def _read_cinema_from_handle(self, handle, width, height):
            for y in range(height):
                line = handle.readline().strip()
                x = 0
                for char in line:
                    self.set(x, y, Item(char))
                    x += 1
                if x != width:
                    err = "row not of length " + str(width) + ": " + line
                    raise InvalidCinema(err)

    def _read_groups_from_handle(self, handle):
        for k, v in zip(range(1, 9), handle.readline().split(' ')):
            if int(v) > 0:
                self.groups[k] = int(v)

    def __str__(self):
        def row_to_str(row):
            return ''.join(map(lambda x: x.value, row))
        return '\n'.join(map(row_to_str, self.grid))

    def score(self):
        # Return sum of guests placed.
        return sum(self._placed_groups())

    def number_of_free_seats(self):
        seats = 0
        for x, y in self.positions():
            if self.at(x, y) == Item.CHAIR:
                seats += 1
        return seats

    def number_of_guests(self):
        num = 0
        for size, n in self.groups.items():
            num += size * n
        return num

    def _count_groups(self, groups):
        group_counts = {}
        for size in self._placed_groups():
            if size in group_counts:
                group_counts[size] += 1
            else:
                group_counts[size] = 1
        return group_counts

    def _placed_groups(self):
        # Count groups placed on the grid
        groups = []
        for y in range(self.height):
            in_group = False
            count = 0
            for x in range(self.width):
                s = self.at(x, y)

                # Four possible cases for S and our state:
                #
                # * in a group, and s is taken -> add one to group size and
                #   continue
                # * in a group and s is not taken ->
                #   we reached the end of a group, save group size.
                # * not in a group, and s is taken -> We have entered a group
                # * not in a group, and s is not taken -> continue
                if not in_group and s is Item.TAKEN:
                    in_group = True
                    count += 1
                elif in_group and s is Item.TAKEN:
                    count += 1
                elif in_group and s is not Item.TAKEN:
                    in_group = False
                    groups.append(count)
                    count = 0

            # catch the case where we are at the end of a line
            if count != 0:
                groups.append(count)
        return groups


class CbcSolver:
    def __init__(self):
        from ortools.linear_solver import pywraplp
        self.pywraplp = pywraplp
        self.solver = pywraplp.Solver('CBC', pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING)
        if verbose_mode:
            self.solver.EnableOutput()

    def add_bool_var(self, name):
        return self.solver.BoolVar(name)

    def add_constraint(self, constraint):
        self.solver.Add(constraint)

    def add_objective(self, objective):
        self.solver.Maximize(objective)

    def solve(self):
        debug('Number of variables:', self.solver.NumVariables())
        debug('Number of constraints:', self.solver.NumConstraints())
        status = self.solver.Solve()
        if status == self.pywraplp.Solver.OPTIMAL:
            debug('Problem solved in {} milliseconds'.format(self.solver.wall_time()))
            debug('Objective value:', self.solver.Objective().Value())
            return True
        else:
            debug('Problem not solved:', status)
            return False

    def group_placed_at_variable(self, var):
        return var.solution_value() == 1


class GurobiSolver:
    def __init__(self, tune=False, params=[], logfile=None):
        import gurobipy
        self.GRB = gurobipy.GRB
        # This is required to keep Gurobi from writing to stdout.
        # However, some versions of gurobipy don't have the `empty` parameter.
        # In this case, "Academic license - for non-commercial use only"
        # (or something similar) will be printed before we can set LogToConsole.
        # See https://support.gurobi.com/hc/en-us/articles/360044784552
        try:
            self.env = gurobipy.Env(empty=True)
        except TypeError:
            self.env = gurobipy.Env()
        if not verbose_mode:
            self.env.setParam('OutputFlag', 0)
        if logfile:
            self.env.setParam('LogFile', logfile)
        self.tune = tune
        self.params = params
        self.solver = gurobipy.Model('Cinema', env=self.env)

    def add_bool_var(self, name):
        return self.solver.addVar(vtype=self.GRB.BINARY, ub=1.0, name=name)

    def add_constraint(self, constraint):
        self.solver.addConstr(constraint)

    def add_objective(self, objective):
        self.solver.setObjective(objective, self.GRB.MAXIMIZE)

    def solve(self):
        if self.tune:
            self.solver.tune()
            for i in range(self.solver.tuneResultCount):
                self.solver.getTuneResult(i)
        else:
            # focus on finding feasible solutions quickly
            self.solver.params.MIPFocus = 1
            # branch variable selection by Pseudo Shadow Price Branching
            self.solver.params.VarBranch = 1
            # other parameters:
            for p, v in self.params:
                try:
                    v = int(v)
                except ValueError:
                    v = float(v)
                self.solver.setParam(p, v)
            self.solver.optimize()
        status = self.solver.status
        if status == self.GRB.OPTIMAL:
            debug('Problem solved in {} seconds'.format(self.solver.runtime))
            debug('Objective value:', self.solver.objVal)
            return True
        else:
            debug('Problem not solved:', status)
            return False

    def group_placed_at_variable(self, var):
        return var.x == 1


class Solver:
    def __init__(self, problem, solver):
        self.problem = problem
        self._groups = copy.copy(problem.groups)
        self.solver = solver

    def solve(self):
        variables = self._build_variables()
        self._build_constraints(variables)
        self._build_objective(variables)
        solved = self.solver.solve()
        if solved:
            return self._to_solution_grid(variables)
        else:
            return None

    def _build_variables(self):
        variables = {}
        for size in self._group_sizes():
            variables[size] = self._build_variables_for_size(size)
        return variables

    def _build_variables_for_size(self, size):
        def symname(size, x, y):
            return "g_{}_{}_{}".format(size, x, y)
        variables = {}
        for x, y in self.problem.positions():
            if self._position_is_valid(size, x, y):
                var = self.solver.add_bool_var(symname(size, x, y))
                variables[(x, y)] = var
        return variables

    def _build_constraints(self, variables):
        self._build_constraints_number_of_groups(variables)
        # Use many shapes, even if they are redundant
        self._build_constraints_distance(variables)
        self._build_constraints_distance_T(variables)
        self._build_constraints_distance_flipped_T(variables)

    def _build_constraints_number_of_groups(self, variables):
        for size, vs in variables.items():
            self.solver.add_constraint(sum(vs.values()) <= self._groups[size])

    def _build_constraints_distance(self, variables):
        #  XXX
        # XX#XX
        #  XXX
        # If someone is in the center, all other fields are free.
        # In any case, there can't be more than 4 groups in the shape.
        for x, y in self.problem.positions():
            budget = 4
            groups_in_shape = 0
            for size, vs in variables.items():
                # groups that would be in the center
                for dx in range(1 - size, 1):
                    var = vs.get((x + dx, y))
                    if var is not None:
                        # use up all budget, no others can fit
                        groups_in_shape += budget * var
                # other groups that would be in the shape
                others = []
                # left
                others += [(dx, 0) for dx in range(-1 - size, 1 - size)]
                # right
                others += [(dx, 0) for dx in range(1, 3)]
                # above
                others += [(dx, -1) for dx in range(-size, 2)]
                # below
                others += [(dx,  1) for dx in range(-size, 2)]
                # enforce
                for dx, dy in others:
                    var = vs.get((x+dx, y+dy))
                    if var is not None:
                        groups_in_shape += var
            self.solver.add_constraint(groups_in_shape <= budget)

    def _build_constraints_distance_T(self, variables):
        # This is less obvious.
        # We want to block this area around each used seat:
        #
        #  XXX
        # XX1XX
        #  XXX
        #
        # For that, it's sufficient if there are never more than one group
        # in this simpler shape of neighboring positions:
        #
        #  X#X
        #   X
        #
        # To see why, imagine overlaying this shape in every position where
        # it covers the used seat:
        #
        #                   XXX    XXX
        # XX1 + X1X + XX1 +  1  = XX1XX
        #  X     X     X           XXX
        #
        # Since we only mark the beginning of each group, we also need to
        # widen the shape to the left to also catch the wider groups,
        # e.g. for size 3:
        #
        # XXX#X
        #  XXX
        #
        shape = lambda size: [(dx, 0) for dx in range(-size, 2)] + [(dx, 1) for dx in range(1-size, 1)]
        self._build_constraints_lt_1(variables, shape)

    def _build_constraints_distance_flipped_T(self, variables):
        # Same as above, but flipped:
        #   X
        #  X#X
        shape = lambda size: [(dx, 0) for dx in range(-size, 2)] + [(dx, -1) for dx in range(1-size, 1)]
        self._build_constraints_lt_1(variables, shape)

    def _build_constraints_lt_1(self, variables, shape):
        # enforce the shape everywhere
        for x, y in self.problem.positions():
            groups_in_shape = 0
            for size, vs in variables.items():
                for dx, dy in shape(size):
                    var = vs.get((x + dx, y + dy))
                    if var is not None:
                        groups_in_shape += var
            self.solver.add_constraint(groups_in_shape <= 1)

    def _build_objective(self, variables):
        objective = 0.0
        for size, vs in variables.items():
            objective += sum(size * v for v in vs.values())
        self.solver.add_objective(objective)

    def _to_solution_grid(self, variables):
        solution = copy.deepcopy(self.problem)
        for size, vs in variables.items():
            for pos, var in vs.items():
                x_left, y = pos
                if self.solver.group_placed_at_variable(var):
                    for x in range(x_left, x_left + size):
                        if solution.at(x, y) != Item.CHAIR:
                            err = str(solution.at(x, y)) + " at " + str((x, y))
                            debug("Invalid solution:", err)
                            raise InvalidSolution(err)
                        solution.set(x, y, Item.TAKEN)
        return solution

    def _position_is_valid(self, size, x, y):
        for i in range(size):
            if self.problem.at(x + i, y) != Item.CHAIR:
                return False
        return True

    def _group_sizes(self):
        return self._groups.keys()


def __main__():
    parser = argparse.ArgumentParser(description='Solve a cinema problem using an ILP solver.')
    parser.add_argument(
            '--solver', metavar='SOLVER',
            choices=['cbc','gurobi'],
            action='store', default='gurobi',
            help='available backends: gurobi (default), cbc')
    parser.add_argument(
            '--file', metavar='PATH',
            action='store',
            help='read the problem from a file (default: read it from stdin)')
    parser.add_argument(
            '--tune',
            action='store_const', default=False, const=True,
            help='tune the selected solver')
    parser.add_argument(
            '--gurobi-param',
            action='append', default=[], nargs=2,
            help='parameters for the Gurobi model')
    parser.add_argument(
            '--verbose', '-v',
            action='store_const', default=False, const=True,
            help='enable verbose output')
    args = parser.parse_args()

    global verbose_mode
    verbose_mode = args.verbose

    if args.file:
        with open(args.file, 'r') as handle:
            problem = CinemaProblem(handle)
    else:
        problem = CinemaProblem(sys.stdin)

    debug('Size: ', problem.height, "x", problem.width)
    debug('Number of seats:', problem.number_of_free_seats())
    debug('Number of guests:', problem.number_of_guests())

    if args.solver == 'cbc':
        backend = CbcSolver()
    elif args.solver == 'gurobi':
        backend = GurobiSolver(tune=args.tune, params=args.gurobi_param) # args.file+'.gurobi.log')
    solver = Solver(problem, backend)

    solution = solver.solve()
    if solution is None:
        print("No solution found")
    else:
        debug("Optimal solution found!")
        debug('Number of seated guests:', solution.score())
        print(solution)

if __name__ == "__main__":
    __main__()
