FROM mcr.microsoft.com/dotnet/core/sdk:3.1

WORKDIR /opt/cinema

COPY ["Cinema Assignment","/opt/cinema"]

RUN dotnet restore
RUN dotnet build -c Release
CMD "/opt/cinema/Cinema Assignment/bin/Release/netcoreapp3.1/Cinema Assignment"

