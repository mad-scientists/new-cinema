Launch the Cinema Assignment with 2 input arguments(strings):
	-the cinema (including groups to place)
	-which solver to use

For a reference to how the file should look like, see any file in the "testcases" folder.
Example: "../../../../../testcases/reference/offline/Exact01.txt"

The possible solver to choose from as second argument:
	- "online-greedy"
	- "online-naive"
	- "off-bb"
	- "off-ilp"
NOTE: for the "off-ilp" solver, there are certain packages required, run the following commands to install them from the commandline (directory should be this root directory):

virtualenv -p python3 .venv
source .venv/bin/activate
pip install -r ./requirements.txt