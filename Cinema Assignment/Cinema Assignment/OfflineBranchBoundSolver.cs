﻿using System;
using System.Collections.Generic;

namespace Cinema_Assignment
{
    public class GroupCollection
    {
        public GroupCollection()
        {
            Groups = new int[8];
            for (int i = 0; i < 8; i++)
            {
                Groups[i] = 0;
            }
        }

        public GroupCollection(Groups groups)
        {
            Groups = new int[8];
            for (int i = 0; i < 8; i++)
            {
                Groups[i] = groups.Dict[new Group(i + 1)];
            }
        }

        public GroupCollection(GroupCollection copy)
        {
            Groups = new int[8];
            for (int i = 0; i < 8; i++)
            {
                Groups[i] = copy.Groups[i];
            }
        }

        public int[] Groups { get; }
    }

    public struct Placement
    {
        public Position Pos { get; set; }
        public int Size { get; set; }

        public bool IsEqualTo(Placement p)
        {
            return p.Pos.X == Pos.X && p.Pos.Y == Pos.Y && p.Size == Size;
        }

        public override int GetHashCode()
        {
            // Unique hash for this placement since Size <= 8, X <= 1000 and Y <= 1000;
            return Size + Pos.X * 10 + Pos.Y * 10_000;
        }
    }

    public class PlacementOrderComparer : IComparer<Placement>
    {
        public int Compare(Placement a, Placement b)
        {
            int cmp = a.Pos.X.CompareTo(b.Pos.X);

            if (cmp != 0)
            {
                return cmp;
            }

            cmp = a.Pos.Y.CompareTo(b.Pos.Y);

            if (cmp != 0)
            {
                return cmp;
            }

            return a.Size.CompareTo(b.Size);
        }
    }

    public class Solution
    {
        // A solution is a collection of groups placed so far and how many people there are left.
        // We cache the score and a hash if itself.
        // We store solutions in a HashSet to check if this configuration has already been evaluated
        // to prevent evaluation of configurations we have already explored. Because we use a HashSet
        // a unique hash is needed.

        private int hash;

        public Solution(GroupCollection groups)
        {
            hash = 0;
            Score = 0;
            Placements = new List<Placement>();
            GroupsLeft = new GroupCollection(groups);
        }

        public Solution(Solution parent)
        {
            hash = parent.hash;
            Score = parent.Score;
            Placements = new List<Placement>(parent.Placements);
            GroupsLeft = new GroupCollection(parent.GroupsLeft);
        }

        public void AddGroup(Position pos, int size_idx)
        {
            Placement placement = new Placement() { Pos = pos, Size = size_idx + 1 };
            int idx = Placements.BinarySearch(placement, new PlacementOrderComparer());

            if (idx < 0)
            {
                Placements.Insert(~idx, placement);
            }
            else
            {
                Placements.Insert(idx, placement);
            }

            Score += size_idx + 1;
            GroupsLeft.Groups[size_idx]--;

            // The placements of the solution are ordered so progressively hashing
            // over them should result in a unique(ish) hash. Hopefully this
            // hashing scheme works good enough.
            // yoinked from here https://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-overriding-gethashcode/263416#263416
            // We calculate it here so, that we can cache the hash value and we don't have
            // to calculate it every time.
            hash = 17;
            foreach (var p in Placements)
            {
                hash = 23 * hash + p.GetHashCode();
            }
        }

        public int Score { get; private set; }
        public GroupCollection GroupsLeft { get; }
        public List<Placement> Placements { get; }

        public override int GetHashCode()
        {
            return hash;
        }
    }

    public class SolutionScoreComparer : IComparer<Solution>
    {
        public int Compare(Solution x, Solution y)
        {
            return x.Score.CompareTo(y.Score);
        }
    }

    public class SolutionEqualityComparer : IEqualityComparer<Solution>
    {
        // Needed for the HashSet.

        public bool Equals(Solution x, Solution y)
        {
            if (x.Placements.Count == y.Placements.Count)
            {
                for (int i = 0; i < x.Placements.Count; i++)
                {
                    if (!x.Placements[i].IsEqualTo(y.Placements[i]))
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        public int GetHashCode(Solution x)
        {
            return x.GetHashCode();
        }
    }

    class OfflineBranchBoundSolver : OfflineSolver
    {
        Cinema problem;

        Solution bestSolution;

        List<Solution> sortedSolutions;
        HashSet<Solution> solutionSet;

        public OfflineBranchBoundSolver(Cinema cinema, Groups groups) : base(cinema, groups)
        {
            problem = cinema;

            bestSolution = new Solution(new GroupCollection(groups));

            sortedSolutions = new List<Solution>();
            solutionSet = new HashSet<Solution>(new SolutionEqualityComparer());

            sortedSolutions.Add(bestSolution);
            solutionSet.Add(bestSolution);
        }

        /// <summary>
        /// Initial call for Solve. Prepares the HashSet used for solving the problem.
        /// </summary>
        /// <param name="cinema"></param>
        /// <returns></returns>
        public override Dictionary<Position, Group> Solve()
        {
            SolutionScoreComparer comparer = new SolutionScoreComparer();

            int already_evaluated = 0;
            int skipped = 0;
            while (sortedSolutions.Count > 0)
            {

                // use the sub-solution with the highest score
                Solution best = sortedSolutions[sortedSolutions.Count - 1];
                sortedSolutions.RemoveAt(sortedSolutions.Count - 1);

                Cinema cinema = new Cinema(problem, best);

                // check if it can become better than the current best solution
                if (best.Score + cinema.AvailableSeats() <= bestSolution.Score)
                {
                    skipped++;
                    continue;
                }

                // We try to place the biggest groups first to quickly get a decent score so that we can cut branches faster
                for (int i = 7; i-- > 0;)
                {
                    if (best.GroupsLeft.Groups[i] == 0)
                    {
                        continue;
                    }

                    Group group = new Group(i + 1);

                    for (int y = 0; y < cinema.Height; y++)
                    {
                        for (int x = 0; x < cinema.Width; x++)
                        {
                            Position pos = new Position(x, y);
                            if (cinema.ValidPosition(group, pos))
                            {
                                // We build a new sub-solution for this branch.
                                Solution child = new Solution(best);
                                child.AddGroup(pos, i);

                                // We have already seen this solution so we don't add it to our active list
                                if (solutionSet.Contains(child))
                                {
                                    already_evaluated++;
                                    continue;
                                }

                                // Add this subsolution to our set so that we don't add it a second time and needlessly evaluate again.
                                solutionSet.Add(child);

                                // Insert the new child solution to our active list
                                // We do a binary search and insert it so that our active list stays sorted.
                                int idx = sortedSolutions.BinarySearch(child, comparer);
                                if (idx < 0)
                                {
                                    sortedSolutions.Insert(~idx, child);
                                }
                                else
                                {
                                    sortedSolutions.Insert(idx, child);
                                }

                                if (child.Score > bestSolution.Score)
                                {
                                    // Keep track of our best solution
                                    bestSolution = child;
                                }
                            }
                        }
                    }
                }
            }

            Dictionary<Position, Group> d = new Dictionary<Position, Group>();
            foreach (var p in bestSolution.Placements)
            {
                d.Add(p.Pos, new Group(p.Size));
            }

            return d;
        }
    }
}
