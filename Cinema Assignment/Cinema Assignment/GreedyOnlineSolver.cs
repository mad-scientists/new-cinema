using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema_Assignment
{
    class CinemaPlacer
    {
        byte[][] seats;

        private const byte Free = 1; // This must be one so that we can calculate cost easily
        private const byte Unavailable = 0;

        public CinemaPlacer(Cinema cinema)
        {
            Width = cinema.Width;
            Height = cinema.Height;

            seats = new byte[cinema.Height][];
            for (int i = 0; i < cinema.Height; i++)
            {
                seats[i] = new byte[cinema.Width];
            }

            for (int y = 0; y < cinema.Height; y++)
            {
                for (int x = 0; x < cinema.Width; x++)
                {
                    seats[y][x] = (cinema.Seats[y][x] == Node.Chair) ? Free : Unavailable;
                }
            }
        }

        public bool CanPlace(Position pos, int groupSize)
        {
            // I assume that most of the time this position will not be available so this is
            // meant as an early out. Can be removed if so desired.
            if (seats[pos.Y][pos.X] == Unavailable)
            {
                return false;
            }

            // Make sure everything is in bounds
            if (pos.Y < 0 || pos.Y >= Height || pos.X < 0 || pos.X >= Width || pos.X + groupSize > Width)
            {
                return false;
            }

            for (int i = 0; i < groupSize; i++)
            {
                if (seats[pos.Y][pos.X + i] == Unavailable)
                {
                    return false;
                }
            }

            return true;
        }

        public int PlacementCost(Position pos, int groupSize)
        {
            // It is assumed that it is possible to place a group of the provided size at the
            // provided position.

            int cost = 0;

            int miny = Math.Max(0, pos.Y - 1);
            int maxy = Math.Min(Height, pos.Y + 2);

            int minx = Math.Max(0, pos.X - 1);
            int maxx = Math.Min(Width, pos.X + groupSize + 1);

            // All the seats directly above and below the group and the seats where the group will sit.
            for (int y = miny; y < maxy; y++)
            {
                for (int x = minx; x < maxx; x++)
                {
                    cost += seats[y][x];
                }
            }

            if (pos.X - 2 >= 0) { cost += seats[pos.Y][pos.X - 2]; }

            if (pos.X + groupSize + 1 < Width) { cost += seats[pos.Y][pos.X + groupSize + 1]; }

            return cost;
        }

        public void Place(Position pos, int groupSize)
        {
            int miny = Math.Max(0, pos.Y - 1);
            int maxy = Math.Min(Height, pos.Y + 2);

            int minx = Math.Max(0, pos.X - 1);
            int maxx = Math.Min(Width, pos.X + groupSize + 1);

            for (int y = miny; y < maxy; y++)
            {
                for (int x = minx; x < maxx; x++)
                {
                    seats[y][x] = Unavailable;
                }
            }

            if (pos.X - 2 >= 0) { seats[pos.Y][pos.X - 2] = Unavailable; }

            if (pos.X + groupSize + 1 < Width) { seats[pos.Y][pos.X + groupSize + 1] = Unavailable; }
        }

        public int Height { get; }
        public int Width { get; }

        public string ToAscii()
        {
            StringBuilder builder = new StringBuilder();
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    builder.Append((seats[y][x] == Unavailable) ? '-' : 'x');
                }
                builder.Append(System.Environment.NewLine);
            }

            return builder.ToString();
        }
    }

    class GreedyOnlineSolver : OnlineSolver
    {
        CinemaPlacer placer;
        Dictionary<Position, int> solution;

        public GreedyOnlineSolver()
        {
            solution = new Dictionary<Position, int>();
        }

        public override void Init(Cinema cinema)
        {
            placer = new CinemaPlacer(cinema);
        }

        public override Position? PlaceGroup(Group group)
        {
            int groupSize = group.Size;
            Position? bestPlacement = null;
            int bestCost = int.MaxValue;

            int bestPossibleCost = groupSize;

            // Naively iterate over all possible positions and compute their score
            // keeping track of the best one.
            for (int y = 0; y < placer.Height; y++) {
                for (int x = 0; x < placer.Width; x++) {
                    Position pos = new Position(x, y);
                    if (placer.CanPlace(pos, groupSize)) {
                        int cost = placer.PlacementCost(pos, groupSize);
                        if (cost < bestCost) {
                            bestCost = cost;
                            bestPlacement = pos;

                            if (bestCost == bestPossibleCost)
                            {
                                goto FoundBest;
                            }
                        }
                    }
                }
            }

            FoundBest:

            if (bestPlacement != null) {
                placer.Place(bestPlacement.Value, groupSize);
                solution.Add(bestPlacement.Value, groupSize);
            }

            return bestPlacement;
        }
    }
}
