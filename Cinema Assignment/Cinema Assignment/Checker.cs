﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cinema_Assignment
{
    [System.Serializable]
    public class CheckerException : System.Exception
    {
        public CheckerException(string message) : base(message) { }
    }

    class Checker
    {
        Cinema problem;
        Groups groups;
        Cinema solution;
        int width, height;

        public Checker(Cinema problem, Groups groups, Cinema solution)
        {
            this.problem = problem;
            this.groups = groups;
            this.solution = solution;
            this.height = problem.Seats.Length;
            this.width = problem.Seats[0].Length;
        }

        /// <summary>
        /// Verifies the cinema. Prints the exception message if it doesn't fulfill the requirements.
        /// </summary>
        /// <returns>True if the cinema is valid, false otherwise.</returns>
        public bool Verify()
        {
            try
            {
                VerifyGridDims();
                VerifyValidPositions();
                VerifyVertical();
                VerifyHorizontal();
                VerifyGroups();
            }
            catch (CheckerException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Checks if the problem and solution are the correct size.
        /// </summary>
        /// <returns>Throws and Exception or returns True.</returns>
        bool VerifyGridDims()
        {
            // check problem
            if (!(problem.Seats.Length == height
                && Array.TrueForAll(problem.Seats, row => row.Length == width)))
                throw new CheckerException("Problem dimensions do not match provided dimensions");

            // check solution
            if (!(solution.Seats.Length == height
                && Array.TrueForAll(solution.Seats, row => row.Length == width)))
                throw new CheckerException("Solution dimensions do not match provided dimensions.");

            return true;
        }

        /// <summary>
        /// Checks if there are no Taken chairs on invalid positions.
        /// </summary>
        /// <returns>Throws and Exception or returns True.</returns>
        bool VerifyValidPositions()
        {
            Parallel.For(0, height, y =>
            {
                for (int x = 0; x < width; x++)
                {
                    Node p = problem.Seats[y][x];
                    Node s = solution.Seats[y][x];

                    switch (p)
                    {
                        case Node.Empty:
                            if (s != Node.Empty)
                                throw new CheckerException("Empty place in problem is no longer empty");
                            break;
                        case Node.Chair:
                            if (s != Node.Chair && s != Node.Taken && s!= Node.Unavailable)
                                throw new CheckerException("Chair is not longer in a valid state.");
                            break;
                        case Node.Taken:
                            if (s != Node.Taken)
                                throw new CheckerException("Taken seat is not longer taken");
                            break;
                    }
                }
            });

            return true;
        }

        /// <summary>
        /// Checks if there are vertically and diagonally no taken chairs too close to eachother.
        /// </summary>
        /// <returns>Throws and Exception or returns True.</returns>
        bool VerifyVertical()
        {
            Parallel.For(0, height, y =>
            {
                for (int x = 0; x < width; x++)
                {
                    if (solution.Seats[y][x] != Node.Taken)
                        continue;
                    int[] horizontal = new int[3] { -1, 0, 1 };
                    int[] vertical = new int[2] { -1, 1 };
                    foreach (int h in horizontal)
                    {
                        if (x + h < 0 || x + h >= width)
                            continue;
                        foreach (int v in vertical)
                        {
                            if (y + v < 0 || y + v >= height)
                                continue;
                            if (solution.Seats[y + v][x + h] == Node.Taken)
                                throw new CheckerException("Vertically too close to another person");
                        }
                    }
                }
            });
            return true;
        }

        /// <summary>
        /// Checks if there are horizontally no taken chairs too close to eachother.
        /// </summary>
        /// <returns>Throws and Exception or returns True.</returns>
        bool VerifyHorizontal()
        {
            Parallel.For(0, height, y =>
            {
                for (int x = 0; x+2 < width; x++)
                {
                    // only look to the right, earlier iterations check the left.
                    // only check if there is a gap of size 1, it's a group otherwise.
                    Node s0 = solution.Seats[y][x];
                    Node s1 = solution.Seats[y][x+1];
                    Node s2 = solution.Seats[y][x+2];

                    if (s0 == Node.Taken && s1 != Node.Taken && s2 == Node.Taken)
                        throw new CheckerException("Horizontally too close to another person");
                }
            });
            return true;
        }

        /// <summary>
        /// Checks if all groups in the solution are also in the input.
        /// </summary>
        /// <returns>Throws and Exception or returns True.</returns>
        bool VerifyGroups()
        {
            foreach (KeyValuePair<Group, int> kv in solution.PlacedGroups().Dict)
            {
                if (!groups.Dict.ContainsKey(kv.Key)
                    || groups.Dict[kv.Key] < kv.Value)
                    throw new CheckerException("Found a group not in the input");
            }

            return true;
        }
    }
}
