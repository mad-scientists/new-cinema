﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema_Assignment
{
    abstract class OnlineSolver
    {
        public abstract void Init(Cinema cinema);

        public abstract Position? PlaceGroup(Group group);
    }
}
