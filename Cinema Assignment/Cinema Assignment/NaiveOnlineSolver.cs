﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema_Assignment
{
    class NaiveOnlineSolver : OnlineSolver
    {
        CinemaPlacer placer;
        Dictionary<Position, int> solution;

        public NaiveOnlineSolver() {
            solution = new Dictionary<Position, int>();
        }

        public override void Init(Cinema cinema) {
            placer = new CinemaPlacer(cinema);
        }

        public override Position? PlaceGroup(Group group) {
            int groupSize = group.Size;
            Position? bestPlacement = null;

            for (int y = 0; y < placer.Height; y++) {
                for (int x = 0; x < placer.Width; x++) {
                    Position pos = new Position(x, y);
                    if (placer.CanPlace(pos, groupSize)) {
                        bestPlacement = pos;
                        goto FoundBest;
                    }
                }
            }

        FoundBest:

            if (bestPlacement != null) {
                placer.Place(bestPlacement.Value, groupSize);
                solution.Add(bestPlacement.Value, groupSize);
            }

            return bestPlacement;
        }
    }
}
