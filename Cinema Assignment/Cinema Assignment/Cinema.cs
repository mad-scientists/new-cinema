﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Cinema_Assignment
{
    /// <summary>
    /// Representation of a Node in the grid. 
    /// Can be: Empty, Chair or Taken.
    /// </summary>
    enum Node
    {
        Empty = '0',
        Chair = '1',
        Taken = 'x',
        Unavailable = '-'
    }

    public readonly struct Position
    {
        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }
        public int X { get; }
        public int Y { get; }
    }

    public readonly struct Group
    {
        public Group(int size)
        {
            Size = size;
        }
        public int Size { get; }
    }

    public class Groups
    {
        public Groups(Dictionary<Group, int> dict)
        {
            Dict = dict;
        }

        public Groups(IEnumerable<Group> groups)
        {
            Dict = new Dictionary<Group, int>();
            foreach (Group group in groups)
            {
                if (Dict.ContainsKey(group))
                {
                    Dict[group]++;
                }
                else
                {
                    Dict.Add(group, 1);
                }
            }
        }

        public Groups(TextReader reader)
        {
            Dict = new Dictionary<Group, int>();
            string[] line = reader.ReadLine().Split(' ');
            for (int i = 0; i < line.Length; i++)
            {
                Dict.Add(new Group(size: i + 1), int.Parse(line[i]));
            }
        }

        public int Score()
        {
            int score = 0;
            foreach (var item in Dict)
            {
                score += item.Key.Size * item.Value;
            }
            return score;
        }

        public string ToAscii()
        {
            string result = "";
            foreach (KeyValuePair<Group, int> kv in Dict)
            {
                result += kv.Value + " ";
            }
            return result.Trim();
        }

        public Dictionary<Group, int> Dict { get; }
    }

    class Cinema
    {
        // The seats in the cinema. Location: [y][x]
        Node[][] seats;

        /// <summary>
        /// Constructor of a Cinema object.
        /// </summary>
        /// <param name="inputFile">Name of the input file, should include ".txt".</param>
        /// <param name="online">Check which problem we're solving.</param>
        public Cinema(TextReader reader, int? height = null, int? width = null)
        {
            Height = height ?? int.Parse(reader.ReadLine());
            Width = width ?? int.Parse(reader.ReadLine());

            seats = new Node[Height][];
            for (int i = 0; i < Height; i++)
            {
                seats[i] = new Node[Width];

                // read the initial chair status
                string line = reader.ReadLine();
                for (int j = 0; j < Width; j++)
                {
                    seats[i][j] = (Node)line[j];
                }
            }
        }
        /// <summary>
        /// Construct a cinema from just the layout as string.
        /// </summary>
        /// <param name="cinema"></param>
        public Cinema(string cinema)
        {
            string[] lines = cinema.Split("\r\n", System.StringSplitOptions.RemoveEmptyEntries);
            Height = lines.Length;
            Width = lines[0].Length;

            seats = new Node[Height][];
            for (int i = 0; i < Height; i++)
            {
                seats[i] = new Node[Width];

                for (int j = 0; j < Width; j++)
                {
                    seats[i][j] = (Node)lines[i][j];
                }
            }
        }
        /// <summary>
        /// Make a copy instance of the given Cinema.
        /// </summary>
        /// <param name="c"></param>
        public Cinema(Cinema c, Dictionary<Position, Group> groups)
        {
            this.Height = c.Height;
            this.Width = c.Width;
            this.seats = new Node[Height][];
            for (int i = 0; i < c.Seats.Length; i++)
            {
                this.seats[i] = new Node[Width];
                c.Seats[i].CopyTo(this.seats[i], 0);
            }
            foreach (KeyValuePair<Position, Group> kv in groups)
            {
                Place(kv.Value, kv.Key);
            }
        }
        public Cinema(Cinema c, Solution solution)
        {
            Height = c.Height;
            Width = c.Width;
            seats = new Node[Height][];
            for (int i = 0; i < Height; i++)
            {
                seats[i] = new Node[Width];
                c.seats[i].CopyTo(seats[i], 0);
            }

            foreach (var placement in solution.Placements)
            {
                Place(new Group(placement.Size), placement.Pos);
            }
        }

        Node At(int x, int y)
        {
            if (x < 0 || x >= Width || y < 0 || y >= Height)
                return Node.Empty;
            else 
                return seats[y][x];
        }
        /// <summary>
        /// Renders the cinema in ASCII, similar to the input it was read from.
        /// </summary>
        public string ToAscii()
        {
            StringBuilder ascii = new StringBuilder();
            for (int y = 0; y < seats.Length; y++)
            {
                for (int x = 0; x < seats[y].Length; x++)
                {
                    ascii.Append((char)seats[y][x]);
                };
                ascii.Append(System.Environment.NewLine);
            }
            return ascii.ToString();
        }

        /// <summary>
        /// Finds what groups have been placed in the current solution.
        /// </summary>
        /// <returns>Dictionary of placed groups of format: (GroupSize, Count)</returns>
        public Groups PlacedGroups()
        {
            List<Group> groups = new List<Group>();

            for(int y = 0; y < seats.Length; y++)
            {
                Position? currentGroup = null;
                int size = 0;
                for (int x = 0; x < seats[y].Length; x++)
                {
                    Node s = At(x,y);

                    if (currentGroup == null && s == Node.Taken)
                    {
                        currentGroup = new Position(x, y);
                        size++;
                    }
                    else if (currentGroup != null && s == Node.Taken)
                        size++;
                    if (currentGroup != null && (s != Node.Taken || x + 1 == seats[y].Length))
                    {
                        groups.Add(new Group(size));
                        currentGroup = null;
                        size = 0;
                    }
                }
            };

            return new Groups(groups);
        }

        /// <summary>
        /// Check if a position is valid for the group.
        /// </summary>
        /// <param name="g">The group for wich the position will be checked.</param>
        /// <param name="p">The position that is checked.</param>
        /// <returns>True if the groups is allowed on the position, False otherwise.</returns>
        public bool ValidPosition(Group g, Position p)
        {
            // check if the group fits.
            if (p.X + g.Size > Width)
                return false;

            // check if the seats for the group are available
            for (int i = 0; i < g.Size; i++)
            {
                if (At(p.X + i, p.Y) != Node.Chair)
                    return false;
            }
            // check if the seats for the group are horizontally not too close to another group.
            Node[] check = new Node[4] {
                At(p.X-2, p.Y), 
                At(p.X-1, p.Y), 
                At(p.X+1+g.Size, p.Y), 
                At(p.X+2+g.Size, p.Y)};
            if (check.Contains(Node.Taken))
                return false;

            // check if the seats for the group are vertically not too close to another group.
            int[] horizontal = new int[3] { -1, 0, 1 };
            int[] vertical = new int[2] { -1, 1 };
            foreach (int x in horizontal)
            {
                foreach (int y in vertical)
                {
                    // check if a position is taken.
                    for (int i = 0; i < g.Size; i++)
                    {
                        if (At(p.X + x + i, p.Y + y) == Node.Taken)
                            return false;
                    }
                }
            }
            return true;
        }
        /// <summary>
        /// SHOULD BE REMOVED DURING REFACTOR. Place a group in the current cinema at a position.
        /// </summary>
        /// <param name="g">The group that is placed.</param>
        /// <param name="p">The position where the group is placed.</param>
        public void Place(Group g, Position p)
        {
            // loop over the row of the group
            for (int i = -2; i < g.Size+2; i++)
            {
                // only change available seats
                if (At(p.X + i, p.Y) != Node.Chair)
                    continue;
                if (i >= 0 && i < g.Size)
                    seats[p.Y][p.X + i] = Node.Taken;
                else
                    seats[p.Y][p.X + i] = Node.Unavailable;
            }
            // loop over the row above and below it and change nodes to Unavailable
            int[] horizontal = new int[3] { -1, 0, 1 };
            int[] vertical = new int[2] { -1, 1 };
            foreach (int x in horizontal)
            {
                foreach (int y in vertical)
                {
                    for (int i = 0; i < g.Size; i++)
                    {
                        // only change available seats
                        if (At(p.X + x + i, p.Y + y) != Node.Chair)
                            continue;
                        else
                            seats[p.Y + y][p.X + x + i] = Node.Unavailable;
                    }
                }
            }
        }

        /// <summary>
        /// Changes all Unavailable nodes to empty Chairs.
        /// </summary>
        public void ToOutput()
        {
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    if (At(x, y) == Node.Unavailable)
                        seats[y][x] = Node.Chair;
                }
            }
        }

        /// <param name="groups">Should be the currently placed groups, defaults to all groups.</param>
        /// <returns>How many seats were assigned.</returns>
        public int Score()
        {
            return this.PlacedGroups().Score();
        }
        public int AvailableSeats()
        {
            int count = 0;
            for (int y = 0; y < seats.Length; y++)
            {
                for (int x = 0; x < seats[y].Length; x++)
                {
                    if (At(x, y) == Node.Chair)
                        count++;
                }
            }
            return count;
        }

        public Node[][] Seats { get { return seats; } }
        public int Height { get; }
        public int Width { get; }
    }
}
