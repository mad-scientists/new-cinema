﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;

namespace Cinema_Assignment
{
    class Program
    {
        static void Main(string[] args)
        {
            Cinema problem  = null;
            Cinema solution = null;
            Groups groups   = null;

            string filename;
            string solverType;

            filename = args[0];
            solverType = args[1];

            if (solverType == "online-greedy")
            {
                GreedyOnlineSolver solver = new GreedyOnlineSolver();
                Dictionary<Position, Group> placements = new Dictionary<Position, Group>();

                int[] group_counts = new int[8];
                for (int i = 0; i < 8; i++)
                {
                    group_counts[i] = 0;
                }

                int num_placed = 0;
                using (StreamReader reader = new StreamReader(filename))
                {
                    problem = new Cinema(reader);

                    solver.Init(problem);
                    int lowestNotPlaceable = 9;

                    // Some input files don't have each new group on a new line.
                    // Reading the file like this just reads in numbers delimited by whitespace
                    // whether it's a space or a newline or whatever.
                    List<int> groupInput = new List<int>();
                    while (true)
                    {
                        int c = reader.Read();
                        int s = 0;

                        while (c == ' ' || c == '\n' || c == '\t' || c == '\r')
                        {
                            c = reader.Read();
                        }

                        while (c >= '0' && c <= '9')
                        {
                            s = s * 10 + c - '0';
                            c = reader.Read();
                        }

                        if (s == 0 || c == -1)
                        {
                            break;
                        }

                        groupInput.Add(s);
                    }

                    for (int i = 0; i < groupInput.Count; i++)
                    {
                        int groupSize = groupInput[i];
                        if (groupSize == 0)
                        {
                            break;
                        }

                        if (groupSize >= lowestNotPlaceable) continue;

                        group_counts[groupSize - 1]++;
                        
                        Position? pos = solver.PlaceGroup(new Group(groupSize));
                        if (pos == null)
                        {
                            lowestNotPlaceable = groupSize;
                            Console.WriteLine("0, 0");
                        }
                        else
                        {
                            placements.Add(pos.Value, new Group(groupSize));
                            num_placed++;
                            Console.WriteLine($"{pos.Value.X + 1}, {pos.Value.Y + 1}");
                        }
                    }
                }

                Dictionary<Group, int> groups_dict = new Dictionary<Group, int>();
                for (int i = 0; i < 8; i++)
                {
                    groups_dict.Add(new Group(i + 1), group_counts[i]);
                }

                groups = new Groups(groups_dict);
                solution = new Cinema(problem, placements);
            }
            else if (solverType == "online-naive") {
                NaiveOnlineSolver solver = new NaiveOnlineSolver();
                Dictionary<Position, Group> placements = new Dictionary<Position, Group>();

                int[] group_counts = new int[8];
                for (int i = 0; i < 8; i++) {
                    group_counts[i] = 0;
                }

                int num_placed = 0;

                using (StreamReader reader = new StreamReader(filename)) {
                    problem = new Cinema(reader);

                    solver.Init(problem);

                    // Some input files don't have each new group on a new line.
                    // Reading the file like this just reads in numbers delimited by whitespace
                    // whether it's a space or a newline or whatever.
                    List<int> groupInput = new List<int>();
                    while (true)
                    {
                        int c = reader.Read();
                        int s = 0;

                        while (c == ' ' || c == '\n' || c == '\t' || c == '\r')
                        {
                            c = reader.Read();
                        }

                        while (c >= '0' && c <= '9')
                        {
                            s = s * 10 + c - '0';
                            c = reader.Read();
                        }

                        if (s == 0 || c == -1)
                        {
                            break;
                        }

                        groupInput.Add(s);
                    }

                    int lowestPlaceable = 9;
                    for (int i = 0; i < groupInput.Count; i++) {

                        int groupSize = groupInput[i];

                        if (groupSize == 0) {
                            break;
                        }

                        if (groupSize >= lowestPlaceable) continue;

                        group_counts[groupSize - 1]++;

                        Position? pos = solver.PlaceGroup(new Group(groupSize));
                        if (pos == null) {
                            lowestPlaceable = groupSize;
                            Console.WriteLine("0, 0");
                        } else {
                            placements.Add(pos.Value, new Group(groupSize));
                            num_placed++;
                            Console.WriteLine($"{pos.Value.X+1}, {pos.Value.Y+1}");
                        }
                    }
                }

                Dictionary<Group, int> groups_dict = new Dictionary<Group, int>();
                for (int i = 0; i < 8; i++) {
                    groups_dict.Add(new Group(i + 1), group_counts[i]);
                }

                groups = new Groups(groups_dict);
                solution = new Cinema(problem, placements);
            } else if (solverType == "off-bb")
            {
                using (StreamReader reader = new StreamReader(filename))
                {
                    problem = new Cinema(reader);
                    groups = new Groups(reader);
                }
                OfflineSolver solver = new OfflineBranchBoundSolver(problem, groups);
                solution = new Cinema(problem, solver.Solve());
                solution.ToOutput();
            }
            else if (solverType == "off-ilp")
            {
                using (StreamReader reader = new StreamReader(filename))
                {
                    problem = new Cinema(reader);
                    groups = new Groups(reader);
                }
                ProcessStartInfo cmdInfo = new ProcessStartInfo();
                // run python from command prompt
                cmdInfo.FileName = "cmd";
                // command for opening the python file
                cmdInfo.Arguments = "/c" + "python cinema-ilp.py";
                // relative location of python file
                cmdInfo.WorkingDirectory = "../../../../../ilp-solver";
                // redirect stdin and stdout
                cmdInfo.RedirectStandardInput = true;
                cmdInfo.RedirectStandardOutput = true;
                cmdInfo.UseShellExecute = false;
                // start the process
                using (Process cmd = Process.Start(cmdInfo))
                {
                    // provide the problem to the code
                    StreamWriter input = cmd.StandardInput;
                    input.WriteLine(problem.Height);
                    input.WriteLine(problem.Width);
                    // remove new line characters from the cinema
                    input.Write(problem.ToAscii()
                        .Replace('/', '\0')
                        .Replace('n', '\0'));
                    input.WriteLine(groups.ToAscii());

                    // close writer to prevent deadlock
                    input.Close();
                    // read the result
                    string result = cmd.StandardOutput.ReadToEnd();

                    // wait for script to finish
                    cmd.WaitForExit();
                    solution = new Cinema(result);
                }
            }
            else
            {
                Console.WriteLine("Please provide which solver to use.");
                return;
            }
        }
    }
}
