#!/usr/bin/env python3
import copy
from enum import Enum
import sys
import z3


class InvalidCinema(Exception):
    def __init__(self, reason=None):
        self.reason = reason

    def __str__(self):
        return self.reason


class Item(Enum):
    # Representing each possible item.
    EMPTY = '0'
    CHAIR = '1'
    TAKEN = 'x'


class Grid:
    def __init__(self, width, height, default_value=[]):
        self.default_value = default_value
        self.rows = height
        self.cols = width
        self.grid = [
            [self.default_value for x in range(width)] for y in range(height)
        ]

    def dims(self):
        return (self.cols, self.rows)

    def at(self, x, y):
        # Abstraction to allow handling out of bounds cases.
        # We assume if we are out of bounds, the position is empty.
        if (x < 0):
            return self.default_value
        if (y < 0):
            return self.default_value

        try:
            return self.grid[y][x]
        except IndexError:
            return self.default_value

    def set(self, x, y, value, places=1):
        for i in range(places):
            self.grid[y][x+i] = value


class ProblemGrid(Grid):
    def __init__(self, representation):
        # Verify each row has the same length.
        if len(set(map(len, representation))) != 1:
            raise InvalidCinema(
                "Rows are of unequal lengths")

        # Get the dimensions
        rows = len(representation)
        cols = len(representation[0])
        super().__init__(cols, rows, default_value=Item.EMPTY)

        # Convert to a better format.
        y = 0
        for row in representation:
            x = 0
            for col in row:
                self.set(x, y, Item(col))
                x += 1
            y += 1

    def __str__(self):
        res = ""
        for row in self.grid:
            res += ''.join(map(lambda x: x.value, row)) + '\n'
        return res


class PossibleGrid(Grid):
    def __init__(self, cols, rows):
        super().__init__(cols, rows, default_value=[])

    def append(self, x, y, value):
        self.set(x, y, self.at(x, y) + [value])


class NewCinema:
    # These will both hold Grid objects.
    problem = None
    solution = None

    # A list of integers, representing all the groups.
    groups = []

    def __init__(self, input_file, online=False):
        self.online = online
        # Read in our test case
        with open(input_file, 'r') as inp:
            # Read in the number of rows and columns.
            n = int(inp.readline())
            m = int(inp.readline())

            self.dims = (m, n)

            # Read in n rows
            i_rows = []
            for row in range(n):
                i_rows.append(inp.readline().strip('\n'))
            self.problem = ProblemGrid(i_rows)

            # Read in the group sizes, represent as dictionary
            self.groups = {}

            # Initialize the dictionary to 0
            for k in range(1, 9):
                self.groups[k] = 0

            if not online:
                # Offline instances provide groups as counts, in order of their
                # value.
                for k, v in zip(
                        range(1, 9),
                        map(int, inp.readline().split(' '))):
                    self.groups[k] = v
            else:
                # Online instances provide them in order of occurrence.
                #
                # To actual solve the online problem properly, you'd need to
                # provide the immediate state and just one group each time you
                # verify.
                #
                # However generalisation can actual allow us to handle groups
                # arriving at the same time, which is interesting.
                for v in map(int, inp.readline().split(' ')):
                    self.groups[v] += 1

            # Try and read in a solution
            s_rows = []
            try:
                for row in range(n):
                    s_rows.append(inp.readline().strip('\n'))
                if s_rows[0] != '':
                    self.solution = ProblemGrid(s_rows)
            except EOFError:
                # No solution provided
                pass

            inp.close()

    def _verify_grid_dims(self):
        if self.dims != self.problem.dims():
            raise InvalidCinema(
                "Problem dimensions do not match provided dimensions")

        if self.has_solution():
            if self.dims != self.solution.dims():
                raise InvalidCinema(
                    "Solution dimensions do not match provided dimensions")

        return True

    def _verify_valid_positions(self):
        if not self.has_solution():
            return True
        # Go through each position in the solution and check a person can be
        # placed there.
        cols, rows = self.dims
        for y in range(rows):
            for x in range(cols):
                p = self.problem.at(x, y)
                s = self.solution.at(x, y)

                # Cases to handle:
                # * problem is empty -> solution must be empty
                # * problem is chair -> solution can be either chair or taken
                # * problem is taken -> solution must be taken
                #
                # The last case lets us handle online cases with the same code.
                if p is Item.EMPTY and s != p:
                    raise InvalidCinema(
                        "Empty place in problem is no longer empty")
                elif p is Item.CHAIR and s not in [Item.CHAIR, Item.TAKEN]:
                    raise InvalidCinema(
                        "Chair is not longer in a valid state.")
                elif p is Item.TAKEN and s is not Item.TAKEN:
                    raise InvalidCinema(
                        "Taken seat is not longer taken")

        return True

    def _verify_vertical(self):
        if not self.has_solution():
            return True
        # We need to look at 6 positions relative to the current block, to see
        # if we are in an invalid state.
        cols, rows = self.dims
        for y in range(rows):
            for x in range(cols):
                if self.solution.at(x, y) is not Item.TAKEN:
                    continue
                for h, v in zip([-1, 0, 1], [-1, 1]):
                    if self.solution.at(x+h, y+v) is Item.TAKEN:
                        print(self.solution)
                        raise InvalidCinema(
                            "Too close to another person {} {}+{} {}+{}"
                            .format(
                                self.solution.at(x, y),
                                x, h, y, v))
        return True

    def _verify_horizontal(self):
        if not self.has_solution():
            return True
        # Go through each position in the solution and check if we aren't
        # violating the rules around horizontal placement.
        cols, rows = self.dims
        for y in range(rows):
            for x in range(cols):
                # Only need to examine the solution, as the previous tests
                # will catch other issues.
                # Also, we only need to look to the right, as otherwise it
                # would have been caught by an earlier iteration.

                s0 = self.solution.at(x, y) is Item.TAKEN
                s1 = self.solution.at(x+1, y) is Item.TAKEN
                s2 = self.solution.at(x+2, y) is Item.TAKEN

                if s0 and not s1 and s2:
                    raise InvalidCinema(
                        "Solution violates spacing rule")

        return True

    def _verify_groups(self):
        # Check if there are any groups not provided that turn up in our
        # solution.

        if not self.has_solution():
            return True

        groups = self._count_groups()
        # Will raise an exception if it encounters a group it shouldn't.
        self._find_remaining_groups(groups)
        return True

    def _count_groups(self):
        # Process an solution / input, and find out how many groups we can see.
        groups = []
        # Go through each line, a build up a list of each group
        cols, rows = self.dims
        for y in range(rows):
            in_group = False
            count = 0
            for x in range(cols):
                s = self.solution.at(x, y)

                # Four possible cases for S and our state:
                #
                # * in a group, and s is taken -> add one to group size and
                #   continue
                # * in a group and s is not taken ->
                #   we reached the end of a group, save group size.
                # * not in a group, and s is taken -> We have entered a group
                # * not in a group, and s is not taken -> continue
                if not in_group and s is Item.TAKEN:
                    in_group = True
                    count += 1
                elif in_group and s is Item.TAKEN:
                    count += 1
                elif in_group and s is not Item.TAKEN:
                    in_group = False
                    groups.append(count)
                    count = 0

            # catch the case where we are at the end of a line
            if count != 0:
                groups.append(count)
        return groups

    def _find_remaining_groups(self, groups):
        # Return a dictionary containing the number of groups that were not
        # placed.

        # Verify each of these groups exist, by comparing it to the list of
        # group sizes.
        validation = copy.copy(self.groups)
        for group in groups:
            if validation[group] <= 0:
                raise InvalidCinema(
                    "Found a group not in the input")
            validation[group] -= 1
        return validation

    def verify(self):
        # Go through and verify this instance.
        try:
            self._verify_grid_dims()
            self._verify_valid_positions()
            self._verify_vertical()
            self._verify_horizontal()
            self._verify_groups()
        except InvalidCinema as message:
            print(message)
            return False

        return True

    def _sum_group(self, group):
        # Figure out how many people are inside this list of group sizes.
        res = 0
        for k, v in group.items():
            res += k*v
        return res

    def score(self):
        # Return sum of people placed, useful as a metric as we want to
        # optimise this.
        remaining = self._find_remaining_groups(
            self._count_groups()
        )

        return self.max_people() - self._sum_group(remaining)

    def max_people(self):
        # maximum number of people
        return self._sum_group(self.groups)

    def has_solution(self):
        # Check if the user provided a solution for us to verify.
        return self.solution is not None


def symname(size, x, y):
    return "g_{}_{}_{}".format(size, x, y)


def rsymname(name):
    size, x, y = list(map(int, str(name).split('_')[1::]))
    return (size, (x, y))


class NewCinemaSolver:
    def __init__(self, problem, timeout=None):
        self.problem = problem
        self.timeout = timeout

    def solve(self, solver='z3'):
        if solver not in ['z3', 'glpk']:
            return None

        possible = self._find_possible()
        annotated = self._annotate_grid(possible)
        constraints = self._get_constraints(annotated)

        solution = self._z3solve(possible, constraints)

        if solution == []:
            return None

        self.problem.solution = copy.deepcopy(self.problem.problem)
        for size, position in solution:
            x, y = position
            self.problem.solution.set(x, y, Item.TAKEN, size)

        return self.problem.solution

    def _z3solve(self, possible, constraints):
        symbols = {}
        solver = z3.Optimize()
        if self.timeout:
            solver.set("timeout", self.timeout)
        optimize = []
        for size, positions in possible.items():
            max_groups_constraint = []
            for position in positions:
                x, y = position
                name = symname(size, x, y)
                symbols[name] = z3.Bool(name)
                term = z3.If(symbols[name], 1, 0)
                max_groups_constraint.append(term)
                optimize.append(term * size)
            solver.add(
                z3.Sum(max_groups_constraint) <= self.problem.groups[size]
            )

        optimize = z3.Sum(optimize)
        # Position constraints
        for k, cons in constraints.items():
            statements = []
            for item in cons:
                size = item[0]
                x, y = item[1]
                name = symname(size, x, y)
                statements.append(symbols[name])
            solver.add(
               z3.Implies(
                   z3.Or(statements),
                   z3.Not(symbols[symname(k[0], k[1][0], k[1][1])])
               )
            )

        z3.set_option("opt.priority", "box")
        solver.maximize(optimize)
        output = []
        if solver.check():
            model = solver.model()
            output = []
            for var in model:
                if model[var]:
                    output.append(rsymname(var))

        return output

    def _find_possible(self):
        # Obtain a dictionary containing all possible positions for each size
        possible = {}
        for size in self._group_sizes():
            possible[size] = self._possible_positions(size)

        return possible

    def _annotate_grid(self, possible):
        # annotate grid - Basically, attach each possible position to the grid
        # points it applies to.
        # Really inefficient implementation of this.
        # This allows us to quickly build the constraints list.
        width, height = self.problem.dims

        grid = PossibleGrid(width, height)
        for size, positions in possible.items():
            for location in positions:
                x, y = location
                for j in range(size):
                    grid.append(x+j, y, (size, location))

        return grid

    def _get_constraints(self, annotated):
        width, height = annotated.dims()

        constraints = {}
        for y in range(height):
            for x in range(width):
                if self.problem.problem.at(x, y) != Item.CHAIR:
                    continue
                # This is all the ones that clash at this position.
                clashes = self._get_relevant(annotated, x, y)
                for pos in annotated.at(x, y):
                    if pos not in constraints:
                        constraints[pos] = []

                    constraints[pos] += \
                        list(filter(lambda p: p != pos, clashes))
        for k, v in constraints.items():
            constraints[k] = set(sorted(v))
        return constraints

    def _get_relevant(self, annotated, x, y):
        # return all the constraints
        positions = [
            (-1, 1), (0, 1), (1, 1),
            (-2, 0), (-1, 0), (0, 0), (1, 0), (2, 0),
            (-1, -1), (0, -1), (1, -1)
        ]
        relevant = []
        for w, h in positions:
            relevant += annotated.at(x+w, y+h)
        return relevant

    def _group_sizes(self):
        # Return a list of group sizes our graph has to consider
        return list(
            map(
                lambda x: x[0],
                self._relevant_groups()
            )
        )

    def _possible_positions(self, size):
        # Find the starting position of all continuous blocks matching the
        # provided size.
        width, height = self.problem.dims
        grid = self.problem.problem

        out = []
        for y in range(height):
            # not very efficient as we can skip blocks we find to not be
            # suitable.
            for x in range(1+width-size):
                found = True
                for j in range(size):
                    if grid.at(x+j, y) != Item.CHAIR:
                        found = False
                        break
                if found is True:
                    out.append((x, y))
        return out

    def _relevant_groups(self):
        # Return a tuple of all groups that aren't 0, along with their value.
        return filter(lambda kv: kv[1] > 0, self.problem.groups.items())


if __name__ == "__main__":
    problem = NewCinema(sys.argv[1])
    if problem.verify():
        solver = NewCinemaSolver(problem)
        if solver.solve('z3') is not None:
            print(problem.solution)
            print(problem.score())
            print(problem.verify())
        else:
            print("No solution?")
    else:
        print("[!] invalid input")
