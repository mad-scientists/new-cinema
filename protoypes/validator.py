#!/usr/bin/env python3
#
# Quick Python implementation to verify solutions for the problem
# Needs to moved to C# and optimised because it'll be bottleneck.
#
# This should be possible in Polynomial time.
#
# To validate a solution to the new cinema problem, the following needs to be
# true:
# * Grid matches the sizes provided
# * The locations chosen are all chairs.
# * No people are located in positions that would be below, above or diagonal
# to an existing person.
# * On the horizontal axis, there can't be 1 empty spaces between people.
# * The input group sizes need to match the groups found
#
# Optimisations:
# * Each of the separate verification stages iterate separately. Combining
# would save some cycles.
# * Quite a few steps can probably be skipped if the input can be assumed to be
# correct.
# * It's possible to remove the need for all the calls to find out how many
# groups are in the cinema, if you just record it.

import copy
import sys
from enum import Enum


class InvalidCinema(Exception):
    def __init__(self, reason=None):
        self.reason = reason

    def __str__(self):
        return self.reason


class Item(Enum):
    # Representing each possible item.
    EMPTY = '0'
    CHAIR = '1'
    TAKEN = 'x'


class Grid:
    def __init__(self, representation):
        # Verify each row has the same length.
        if len(set(map(len, representation))) != 1:
            raise InvalidCinema(
                "Rows are of unequal lengths")

        # Convert to a better format.
        self.grid = []
        for row in representation:
            grid_row = []
            for col in row:
                grid_row.append(Item(col))
            self.grid.append(grid_row)

        # Set the dimensions for quick access.
        self.rows = len(self.grid)
        self.cols = len(self.grid[0])

    def dims(self):
        return (self.cols, self.rows)

    def at(self, x, y):
        # Abstraction to allow handling out of bounds cases.
        # We assume if we are out of bounds, the position is empty.
        try:
            return self.grid[y][x]
        except IndexError:
            return Item.EMPTY


class NewCinema:
    # These will both hold Grid objects.
    problem = None
    solution = None

    # A list of integers, representing all the groups.
    groups = []

    def __init__(self, input_file, online=False):
        self.online = online
        # Read in our test case
        with open(input_file, 'r') as inp:
            # Read in the number of rows and columns.
            n = int(inp.readline())
            m = int(inp.readline())

            self.dims = (m, n)

            # Read in n rows
            i_rows = []
            for row in range(n):
                i_rows.append(inp.readline().strip('\n'))
            self.problem = Grid(i_rows)

            # Read in the group sizes, represent as dictionary
            self.groups = {}

            # Initialize the dictionary to 0
            for k in range(1, 9):
                self.groups[k] = 0

            if not online:
                # Offline instances provide groups as counts, in order of their
                # value.
                for k, v in zip(
                        range(1, 9),
                        map(int, inp.readline().split(' '))):
                    self.groups[k] = v
            else:
                # Online instances provide them in order of occurrence.
                #
                # To actual solve the online problem properly, you'd need to
                # provide the immediate state and just one group each time you
                # verify.
                #
                # However generalisation can actual allow us to handle groups
                # arriving at the same time, which is interesting.
                for v in map(int, inp.readline().split(' ')):
                    self.groups[v] += 1

            # Try and read in a solution
            s_rows = []
            try:
                for row in range(n):
                    s_rows.append(inp.readline().strip('\n'))
                if s_rows[0] != '':
                    self.solution = Grid(s_rows)
            except EOFError:
                # No solution provided
                pass

            inp.close()

    def _verify_grid_dims(self):
        if self.dims != self.problem.dims():
            raise InvalidCinema(
                "Problem dimensions do not match provided dimensions")

        if self.has_solution():
            if self.dims != self.solution.dims():
                raise InvalidCinema(
                    "Solution dimensions do not match provided dimensions")

        return True

    def _verify_valid_positions(self):
        if not self.has_solution():
            return True
        # Go through each position in the solution and check a person can be
        # placed there.
        cols, rows = self.dims
        for y in range(rows):
            for x in range(cols):
                p = self.problem.at(x, y)
                s = self.solution.at(x, y)

                # Cases to handle:
                # * problem is empty -> solution must be empty
                # * problem is chair -> solution can be either chair or taken
                # * problem is taken -> solution must be taken
                #
                # The last case lets us handle online cases with the same code.
                if p is Item.EMPTY and s != p:
                    raise InvalidCinema(
                        "Empty place in problem is no longer empty")
                elif p is Item.CHAIR and s not in [Item.CHAIR, Item.TAKEN]:
                    raise InvalidCinema(
                        "Chair is not longer in a valid state.")
                elif p is Item.TAKEN and s is not Item.TAKEN:
                    raise InvalidCinema(
                        "Taken seat is not longer taken")

        return True

    def _verify_vertical(self):
        if not self.has_solution():
            return True
        # We need to look at 6 positions relative to the current block, to see
        # if we are in an invalid state.
        cols, rows = self.dims
        for y in range(rows):
            for x in range(cols):
                if self.solution.at(x, y) is not Item.TAKEN:
                    continue
                for dy in [-1, 1]:
                    for dx in [-1, 0, 1]:
                        if self.solution.at(x+dx, y+dy) is Item.TAKEN:
                            err = str((x, y)) + " and " + str((x+dx, y+dy))
                            raise InvalidCinema("Violates distance: " + err)
        return True

    def _verify_horizontal(self):
        if not self.has_solution():
            return True
        # Go through each position in the solution and check if we aren't
        # violating the rules around horizontal placement.
        cols, rows = self.dims
        for y in range(rows):
            for x in range(cols):
                # Only need to examine the solution, as the previous tests
                # will catch other issues.
                # Also, we only need to look to the right, as otherwise it
                # would have been caught by an earlier iteration.
                s0 = self.solution.at(x, y) is Item.TAKEN
                s1 = self.solution.at(x+1, y) is Item.TAKEN
                s2 = self.solution.at(x+2, y) is Item.TAKEN
                if s0 and not s1 and s2:
                    err = str((x, y)) + " and " + str((x+2, y))
                    raise InvalidCinema("Horizontal distance: " + err)
        return True

    def _verify_groups(self):
        # Check if there are any groups not provided that turn up in our
        # solution.

        if not self.has_solution():
            return True

        groups = self._count_groups()
        # Will raise an exception if it encounters a group it shouldn't.
        self._find_remaining_groups(groups)
        return True

    def _count_groups(self):
        # Process an solution / input, and find out how many groups we can see.
        groups = []
        # Go through each line, a build up a list of each group
        cols, rows = self.dims
        for y in range(rows):
            in_group = False
            count = 0
            for x in range(cols):
                s = self.solution.at(x, y)

                # Four possible cases for S and our state:
                #
                # * in a group, and s is taken -> add one to group size and
                #   continue
                # * in a group and s is not taken ->
                #   we reached the end of a group, save group size.
                # * not in a group, and s is taken -> We have entered a group
                # * not in a group, and s is not taken -> continue
                if not in_group and s is Item.TAKEN:
                    in_group = True
                    count += 1
                elif in_group and s is Item.TAKEN:
                    count += 1
                elif in_group and s is not Item.TAKEN:
                    in_group = False
                    groups.append(count)
                    count = 0

            # catch the case where we are at the end of a line
            if count != 0:
                groups.append(count)
        return groups

    def _find_remaining_groups(self, groups):
        # Return a dictionary containing the number of groups that were not
        # placed.

        # Verify each of these groups exist, by comparing it to the list of
        # group sizes.
        validation = copy.copy(self.groups)
        for group in groups:
            if validation[group] <= 0:
                raise InvalidCinema(
                    "Found a group not in the input, size " + str(group))
            validation[group] -= 1
        return validation

    def verify(self):
        # Go through and verify this instance.
        try:
            self._verify_grid_dims()
            self._verify_valid_positions()
            self._verify_vertical()
            self._verify_horizontal()
            self._verify_groups()
        except InvalidCinema as message:
            print(message)
            return False

        return True

    def _sum_group(self, group):
        # Figure out how many people are inside this list of group sizes.
        res = 0
        for k, v in group.items():
            res += k*v
        return res

    def score(self):
        # Return sum of people placed, useful as a metric as we want to
        # optimise this.
        remaining = self._find_remaining_groups(
            self._count_groups()
        )

        return self.max_people() - self._sum_group(remaining)

    def max_people(self):
        # maximum number of people
        return self._sum_group(self.groups)

    def has_solution(self):
        # Check if the user provided a solution for us to verify.
        return self.solution is not None




if __name__ == "__main__":
    instance = NewCinema(sys.argv[1])

    valid = instance.verify()
    print("Valid Instance? {}".format(valid))
    print("Includes a soltuion? {}".format(instance.has_solution()))
    if valid and instance.has_solution():
        # Score this solution
        print(
            "Solution managed to place: {} / {} ".format(
                instance.score(),
                instance.max_people()
            )
        )
