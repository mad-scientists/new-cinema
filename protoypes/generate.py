import random
import sys


def distribution(r):
    return random.random() <= r


class GenerateCinema:

    def __init__(self, width, height, ratio=0.9):
        self.width = width
        self.height = height

        # Ratio of Seats to empty spaces.
        self.ratio = ratio

    def generator(self):
        # Return a iterator of problems.
        return self.generate

    def generate(self):

        while True:
            grid = "\n".join([
                "".join([self.place() for col in range(self.width)])
                for row in range(self.height)
            ])
            yield "\n".join([
                str(self.height),
                str(self.width),
                grid,
                " ".join(map(str, self.groups()))
            ])

    def place(self):
        return ['0', '1'][distribution(self.ratio)]

    def groups(self):
        places = self.width * self.height
        # reasonable approximation of how many groups we want to include.
        n_groups = int(places / 8)
        res = [0 for x in range(1, 9)]
        for i in range(n_groups):
            res[int(len(res)*random.triangular(mode=0.1))] += 1
        return res


if __name__ == "__main__":
    generator = GenerateCinema(
        int(sys.argv[1]),
        int(sys.argv[2]),
        ratio=float(sys.argv[3])
    ).generator()
    for problem in generator():
        print(problem)
        break
