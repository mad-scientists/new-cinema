import time
import os
from z3solver import NewCinema, NewCinemaSolver
from multiprocessing import Pool
TIMEOUT = 1000*60*30

import pickle

def problem(file):
    print("starting: "+file)
    p = NewCinema(file)
    solver = NewCinemaSolver(p, timeout=TIMEOUT)
    start = time.time()
    solution = solver.solve()
    end = time.time()
    finished = end - start
    if finished > TIMEOUT/1000:
        finished = None
    score = 0
    if solution is not None:
        score = p.score()
    
    print("ending: "+file)
    return (file, score, finished)


if __name__ == "__main__":
    path = '../testcases/reference/offline'
    queue = []
    for file in os.listdir(path):
        if file in ["Exact17.txt", "Exact18.txt", "Exact19.txt", "Exact20.txt",
                    "Exact21.txt"]:
            continue
        queue.append("{}/{}".format(path, file))
        #p = problem("{}/{}".format(path, file))
        #print(p)
   
    results = []
    with Pool(4) as p:
        results = p.map(problem, queue)

    print(results)
    r = open('res.pickle', 'w')
    pickle.dump(result, r)
    r.close()
